class Sampleable {
  static sample(ISampleable) {
    let out = typeof ISampleable.defaults === 'function' && ISampleable.defaults();

    if (!out) {
      throw new Error('Error generating sample for ', this);
    }

    return out;
  }
}

class FinalJSON {
  /**
   * Follows `MethodTypes` enum
   * @type {number}
   */
  method;

  /**
   * @type {string}
   */
  route;

  /**
   * @type {string}
   */
  desc;

  /**
   * @type {Object}
   */
  reqType;

  /**
   * @type {Object}
   */
  respType;

  /**
   * Category for endpoint. Supports one level of categorization.
   * @type {string}
   */
  category;

  /**
   * Long informational notes for the endpoint.
   * @type {string}
   */
  notes;

  /**
   * @type {Object}
   */
  reqExample;

  /**
   * @type {Object}
   */
  respExample;

  /**
   * @type {number[]}
   */
  statusCodes;
}

class DockData {
  /**
   * Follows `MethodTypes` enum
   * @type {number}
   */
  method;

  /**
   * @type {string}
   */
  route;

  /**
   * @type {string}
   */
  desc;

  /**
   * @type {Sampleable}
   */
  ReqModel;

  /**
   * @type {Sampleable}
   */
  RespModel;

  /**
   * Category for endpoint. Supports one level of categorization.
   * @type {string}
   */
  category;

  /**
   * Long informational notes for the endpoint.
   * @type {string}
   */
  notes;

  /**
   * @type {Object}
   */
  reqExample;

  /**
   * @type {Object}
   */
  respExample;

  /**
   * @type {number[]}
   */
  statusCodes;
}

class ApiDockerMetaData {
  /**
   * Endpoint description
   * @type {string}
   */
  desc;
}

class ApiDockerOptions {
  /**
   * Request model will be displayed in API page.
   * @type {Sampleable} Any object with sample generating properties.
   */
  reqModel;
  /**
   * Response model will be displayed in API page.
   * @type {Sampleable} Any object with sample generating properties.
   */
  respModel;
  /**
   * HTTP status codes.
   * @type {number[]}
   */
  statusCodes;
  /**
   * Metadata of API endpoint
   * @type {ApiDockerMetaData}
   */
  metaData;
  /**
   * Category for endpoint. Supports one level of categorization.
   * @type {string}
   */
  category;
  /**
   * Long informational notes for the endpoint.
   * @type {string}
   */
  notes;
  /**
   * @type {number[]}
   */
  statusCodes;
}

const MethodTypes = {
  GET: 1,
  POST: 2,
  PUT: 3,
  DELETE: 4,
  HEAD: 5,
  PATCH: 6,
  1: 'GET',
  2: 'POST',
  3: 'PUT',
  4: 'DELETE',
  5: 'HEAD',
  6: 'PATCH'
};

module.exports = { MethodTypes, ApiDockerOptions, ApiDockerMetaData, Sampleable, DockData, FinalJSON };
