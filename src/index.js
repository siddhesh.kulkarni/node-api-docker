const { ApiDockerOptions, MethodTypes, DockData, FinalJSON, Sampleable } = require('./models');
const fs = require('fs');

class ApiDocker {
  /**
   * @type {ApiDocker}
   */
  static instance;

  constructor(attr = {}) {
    this.app = attr.app;
    this.jsonFilePath = attr.jsonFilePath;
    this.apiName = attr.apiName;
    this.apiURL = attr.apiURL;
    this.apiDesc = attr.apiDesc;
    this.gettingStartedHTML = attr.gettingStartedHTML;
    this.usageHTML = attr.usageHTML;

    if (!ApiDocker.instance) {
      ApiDocker.instance = this;
    } else {
      throw new Error('Please use `ApiDocker.instance` instead of `new ApiDocker(...)`');
    }

    /* if (!this.app) {
      throw new Error('app must be provided for `ApiDocker`');
    } */

    /**
     * @type {DockData[]}
     */
    this.docData = [];

    /**
     * @type {FinalJSON[]}
     */
    this.finalJSON = [];
  }

  static register(attr = {}) {
    return new ApiDocker(attr);
  }

  /**
   * Decorates api function with documentation properties.
   * @param {string} route 
   * @param {() => Promise<void>} cb 
   * @param {ApiDockerOptions} options 
   */
  get(route, cb, options) {
    this.dockDecorator(MethodTypes.GET, route, cb, options);
  }

  /**
   * Decorates api function with documentation properties.
   * @param {string} route 
   * @param {() => Promise<void>} cb 
   * @param {ApiDockerOptions} options 
   */
  post(route, cb, options) {
    this.dockDecorator(MethodTypes.POST, route, cb, options);
  }

  /**
   * Decorates api function with documentation properties.
   * @param {string} route 
   * @param {() => Promise<void>} cb 
   * @param {ApiDockerOptions} options 
   */
  put(route, cb, options) {
    this.dockDecorator(MethodTypes.PUT, route, cb, options);
  }

  /**
   * Decorates api function with documentation properties.
   * @param {string} route 
   * @param {() => Promise<void>} cb 
   * @param {ApiDockerOptions} options 
   */
  delete(route, cb, options) {
    this.dockDecorator(MethodTypes.DELETE, route, cb, options);
  }

  /**
   * Decorates api function with documentation properties.
   * @param {string} route 
   * @param {() => Promise<void>} cb 
   * @param {ApiDockerOptions} options 
   */
  head(route, cb, options) {
    this.dockDecorator(MethodTypes.HEAD, route, cb, options);
  }

  /**
   * Decorates api function with documentation properties.
   * @param {string} route 
   * @param {() => Promise<void>} cb 
   * @param {ApiDockerOptions} options 
   */
  patch(route, cb, options) {
    this.dockDecorator(MethodTypes.PATCH, route, cb, options);
  }

  /**
   * Decorates api function with documentation properties.
   * @param {number} methodType 
   * @param {string} route 
   * @param {() => Promise<void>} cb 
   * @param {ApiDockerOptions} options 
   * @private
   */
  dockDecorator(methodType, route, cb, options = {}) {
    const meth = MethodTypes[methodType].toLowerCase();

    this.app && this.app[meth](route, cb);

    this.docData.push({
      ReqModel: options.reqModel,
      RespModel: options.respModel,
      desc: (options.metaData) ? options.metaData.desc : '',
      method: meth,
      route,
      category: options.category ? options.category : 'Uncategorized',
      notes: options.notes ? options.notes : '',
      statusCodes: options.statusCodes ? options.statusCodes : []
    });
  }

  /**
   * Generates JSON on given json path
   */
  generateDocJSON() {
    for (const doc of this.docData) {
      let reqSample = {}, respSample = {}, reqExample, respExample;
      if (doc.ReqModel) {
        reqSample = getJSON(doc.ReqModel.sample(doc.ReqModel));
        reqExample = typeof doc.ReqModel.example === 'function' ? doc.ReqModel.example() : reqSample;
      }

      if (doc.RespModel) {
        respSample = getJSON(doc.RespModel.sample(doc.RespModel));
        respExample = typeof doc.RespModel.example === 'function' ? doc.RespModel.example() : respSample;
      }

      this.finalJSON.push({
        reqType: reqSample,
        respType: respSample,
        desc: doc.desc,
        method: doc.method,
        route: doc.route,
        category: doc.category,
        notes: doc.notes,
        statusCodes: doc.statusCodes,
        reqExample: reqExample,
        respExample
      });
    }

    // Only do this on local and push the json file from git.
    fs.writeFile(this.jsonFilePath, JSON.stringify({
      api: {
        name: this.apiName,
        desc: this.apiDesc,
        apiURL: this.apiURL,
        gettingStartedHTML: this.gettingStartedHTML,
        usageHTML: this.usageHTML
      },
      endpoints: this.finalJSON
    }), function (err) {
      if (err) {
        return console.log(err);
      }
      console.log("Done!");
    });
  }
}

const getJSON = (data) => {
  if (Array.isArray(data) && data.length) {
    let typ1 = [getJSON(data[0])];
    return typ1;
  } else if (Array.isArray(data)) {
    let typ1 = [];
    return typ1;
  } else if (data === null) {
    return null;
  }

  const typ = {};
  for (let key in data) {
    if (data.hasOwnProperty(key)) {
      if (data[key] === null) {
        typ[key] = null;
      }
      if (typeof data[key] === 'number') {
        typ[key] = 'number';
      }
      if (typeof data[key] === 'string') {
        typ[key] = 'string';
      }
      if (typeof data[key] === 'boolean') {
        typ[key] = 'boolean';
      }
      if (Array.isArray(data[key]) && data[key].length) {
        typ[key] = [getJSON(data[key][0])];
      } else if (Array.isArray(data[key])) {
        typ[key] = [];
      } else if (typeof data[key] === 'object') {
        typ[key] = getJSON(data[key]);
      }
    }
  }
  return typ;
};

module.exports = { ApiDocker, ApiDockerOptions, MethodTypes, Sampleable };
