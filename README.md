# Node Api Docker

> Document generator for Node JS APIs (Beta Stage), Generate stylish API Documentation html pages within no time!  

## Demo
-  [GlobalAuth Documentation](https://globalauth.herokuapp.com/docs/ "GlobalAuth Documentation")

## Installation
```
$ npm install @siddhesh3/api-docker --save
```

## Register
index.js
```
const { ApiDocker, Sampleable } = require('@siddhesh3/api-docker');

ApiDocker.register({
  app, // Can be an ExpressJS app
  jsonFilePath: __dirname + '/public/docs/api-data.json',
  apiName: 'My Sample API',
  apiDesc: 'Hello world node js API',
  apiURL: 'https://www.prodapilink.com/',
  gettingStartedHTML: `<p>REST API......</p>
  <h5>...</h5>`,
  usageHTML: `
  <p>Define how to use API......</p>
  <p>...</p>`
});
```

## Usage
### Step 1: Define request class
```
class SampleGetAPIEndpointRequest extends Sampleable {
  userId;

  static defaults() {
    return {
      userId: 1
    };
  }

  static example() {
    return {
      userId: 35
    };
  }
}
```
### Step 2: Define response class
```
class SampleGetAPIEndpointResponse extends Sampleable {
  userData;

  static defaults() {
    return {
      userData: {
        id: 1,
        name: ''
      }
    };
  }

  static example() {
    return {
      userData: {
        id: 35,
        name: 'John Doe'
      }
    };
  }
}
```
### Step 3: Use it on an endpoint
```
ApiDocker.instance.get('/sampleGetAPIEndpoint', (request, response) => {
  // some api code
}, {
  metaData: { desc: 'Description about your api endpoint...' },
  statusCodes: [200],
  reqModel: SampleGetAPIEndpointRequest,
  respModel: SampleGetAPIEndpointResponse,
  category: 'Client'
});

```
### Or document it directly
```
ApiDocker.instance.dockDecorator('/sampleGetAPIEndpoint', () => {}, {
  metaData: { desc: 'Description about your api endpoint...' },
  statusCodes: [200],
  reqModel: SampleGetAPIEndpointRequest,
  respModel: SampleGetAPIEndpointResponse,
  category: 'Client'
});
```
### Step 4: Once all documentation is done in the code, generate its JSON
index.js
```
ApiDocker.instance.generateDocJSON();
```
This will generate `api-data.json` file in configured location.

> This will generate the JSON on runtime when node api is started.

### Step 5: Download and use a api-docker viewer
Download a [node-api-viewer](https://gitlab.com/siddhesh.kulkarni/node-api-viewer "A react api-docker viewer") and put `/docs/` folder from viewer in your api's `/public` folder 
& Replace the generated `api-data.json`
